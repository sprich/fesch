function copyToClipboard() {
	let copyText = document.getElementById('websocket_url');

	copyText.select();
	copyText.setSelectionRange(0, 99999);

	document.execCommand('copy');
}
